terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~>2.0"
    }
    uptimerobot = {
      source = "louy/uptimerobot"
      version = "0.5.1"
    }
  }
  required_version = ">= 0.13"
}
