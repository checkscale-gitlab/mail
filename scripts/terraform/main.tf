# Define the provider. In this case we're building in gitlab
provider "digitalocean" {
  token = var.secrets["digitalocean_token"]
}

provider "dns" {}

provider "cloudflare" {
  email   = var.secrets["cloudflare_email"]
  api_token = var.secrets["cloudflare_api_token"]
  #api_key = var.secrets["cloudflare_api_key"]
}

provider "uptimerobot" {
  api_key = var.secrets["uptimerobot_api_key"]
}

data "cloudflare_zones" "primary" {
  filter {
    name = var.domainname
  }
}

data "dns_a_record_set" "home" {
  host = "home.${var.domainname}"
}

data "digitalocean_ssh_key" "acpatt" {
  name = "acpatt"
}

data "uptimerobot_alert_contact" "alerts" {
  friendly_name = "alerts"
}

data "uptimerobot_alert_contact" "backup" {
  friendly_name = "backup"
}

resource "digitalocean_vpc" "mail-vpc" {
  name     = "mail-vpc"
  region   = "lon1"
  ip_range = "172.28.53.0/24"
}

resource "digitalocean_droplet" "mailserver" {
  image  = "ubuntu-18-04-x64"
  name   = "${var.hostname}.${var.domainname}"
  region = "lon1"
  size   = "s-1vcpu-1gb"
  tags = []
  ssh_keys = [data.digitalocean_ssh_key.acpatt.fingerprint]
  vpc_uuid = digitalocean_vpc.mail-vpc.id
}

resource "digitalocean_project" "mailbox" {
  name        = "Mailbox"
  description = "A project to store resources used for the mailbox."
  purpose     = "Mailserver"
  environment = "Production"
  resources   = [
    digitalocean_droplet.mailserver.urn,
    ]
}

resource "digitalocean_firewall" "mailfw" {
  name = "mailportsin-onlyoutfromacpatt"

  droplet_ids = [digitalocean_droplet.mailserver.id]

  dynamic "inbound_rule" {
    for_each = [for rule in local.fw_rules: {
      protocol         = rule.protocol
      port_range      = rule.port_range
      addresses = rule.addresses
    } if rule.direction == "inbound"
    ]
    content {
      protocol = inbound_rule.value.protocol
      port_range = inbound_rule.value.port_range
      source_addresses = inbound_rule.value.addresses
    }
  }

  dynamic "outbound_rule" {
    for_each = [for rule in local.fw_rules: {
      protocol         = rule.protocol
      port_range      = rule.port_range
      addresses = rule.addresses
    } if rule.direction == "outbound"
    ]
    content {
      protocol = outbound_rule.value.protocol
      port_range = outbound_rule.value.port_range
      destination_addresses = outbound_rule.value.addresses
    }
  }
}

resource "uptimerobot_monitor" "monitor" {
  for_each = {for rule in local.fw_rules: 
    rule.name => {
    monitor_type = rule.monitor_type
    port         = rule.port_range
  } if rule.direction == "inbound" && rule.monitor_type != null}
  
  friendly_name = "Mail Monitor - ${each.value.monitor_type} ${each.value.monitor_type == "port" ? each.value.port : ""}"
  type          = each.value.monitor_type
  port          = each.value.monitor_type == "port" ? each.value.port : null
  sub_type      = each.value.monitor_type == "port" ? "custom" : null
  url           = "https://${var.hostname}.${var.domainname}"
  interval      = 300

  alert_contact {
    id = data.uptimerobot_alert_contact.alerts.id
  }
  alert_contact {
    id = data.uptimerobot_alert_contact.backup.id
  }

}

resource "cloudflare_record" "mailserver_records" {
  for_each = {for record in local.dns_records: 
    "${record.name}-${record.type}" => {
    name            = record.name
    type            = record.type
    content         = try(record.content, null)
    data            = try(record.data,null)
    priority         = try(record.priority, null)
    }
  }
  zone_id = data.cloudflare_zones.primary.zones[0].id
  name    = each.value.name
  value   = each.value.content
  priority = each.value.priority
  type    = each.value.type
  data    = each.value.data
}
